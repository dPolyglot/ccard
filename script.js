
let exp = document.getElementById('expCvv');
exp.addEventListener('keyup', () => {
    if(exp.value.length === 3){
        let slash = document.getElementById('slash');
        slash.style.display = 'block';
    }else if(exp.value.length < 3){
        slash.style.display = 'none';
    }
});


let pinFlex = document.querySelector('.pin-flex');

let inputAll = pinFlex.querySelectorAll('input.input-p');

inputAll.forEach((evt) => {
    evt.addEventListener('keypress', (el) =>{

        let helpText = document.getElementById('helpText');
        let isNumber = String.fromCharCode(el.which);

        if(!(/[0-9]/.test(isNumber))){
            el.preventDefault();
            helpText.style.display = 'block';
        }else if(evt.value.length > 0){
            el.preventDefault();
        }
    })
});

